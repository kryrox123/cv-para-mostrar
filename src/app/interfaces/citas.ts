export interface IDatos {
  nombre:string;
  apellido:string;
  correo:string;
  celular:number;
  fecha:string;
  hora:string;
  descripcion:string;
  password:string
}